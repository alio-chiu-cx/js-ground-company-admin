
function preFillDateForm() {

    $('#datepicker input').datepicker({
        format: 'dd-M-yyyy',
        startDate: '-2d',
        endDate: '+3d',
        autoclose: true,
        todayHighlight: true,
        container: '#datepicker',
        allowInputToggle: true,
        keyboardNavigation: true
    });
}