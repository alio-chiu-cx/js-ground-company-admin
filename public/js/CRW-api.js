/*
 * Username Function Start
 */
function getUsernameCallback(username) {
    $("#CMUsername").val(username);
}
function runGetUsername() {
    if (isCRWIntialize()) {
        var callback = this.getUsernameCallback.bind(this);
        CRW.executeJavaCallback("getUsername", null, callback);
    } else {
        $("#CMUsername").val("JSMSUSER");
    }
}
/*
 * Get Work Location
 */
function getFullLocationCallback(result) {
    if (result == null) {
        $("#fullLocation").val(null);
    } else {
        var json = JSON.parse(result.substring(1, result.length - 1));
        var port = getPort(json['env.fullLocation']);
        $("#fullLocation").val(json['env.fullLocation']);
        $('#origin').val(port);
        $('#origin').attr('readonly', true);
    }
}
function runGetFullLocation() {
    if (isCRWIntialize()) {
        var callback = this.getFullLocationCallback.bind(this);
        CRW.executeJavaCallback("getFullLocation", null, callback);
    }
}

function getPort(location) {
    var locationArray = location.split("/");
    return locationArray[1];
}

function showErrorDialog(msg) {
    $('#popupErrorText').text(msg);
    $("#errorModal").css("display","block");
    $("#errorPopUpButton").focus();
}

function showWarningDialog(msg) {
    $('#popupWarningText').text(msg);
    $("#warningModal").css("display","block");
    $("#warningPopUpButton").focus();
}

function getDefaultCarrierFlightCallback(result) {
    if (!isEmpty(result)) {
        var json = JSON.parse(result.substring(1, result.length - 1));
        var operationCarrier = (json['env.workstationID']).split("/")
        $('#operationCarrier').val(operationCarrier[0]);
    }
}

function callGetDefaultCarrierFlight() {
    if (isCRWIntialize()) {
        var callback = this.getDefaultCarrierFlightCallback.bind(this);
        CRW.executeJavaCallback("getWorkstationID", null, callback);
    } 
}

function isCRWIntialize() {
    return typeof CRW !== 'undefined';
}

