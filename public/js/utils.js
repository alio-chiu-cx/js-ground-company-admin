function getQueryParamsToObject() {
    var uri = decodeURI(location.search.substr(1));
    if (!isEmpty(uri)) {
        var chunks = uri.split('&');
        var params = Object();

        for (var i = 0; i < chunks.length; i++) {
            var chunk = chunks[i].split('=');
            if (chunk[0].search("\\[\\]") !== -1) {
                if (typeof params[chunk[0]] === 'undefined') {
                    params[chunk[0]] = [chunk[1]];

                } else {
                    params[chunk[0]].push(chunk[1]);
                }


            } else {
                params[chunk[0]] = chunk[1];
            }
        }

        return params;
    } else {
        return null;
    }
}

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null)
        return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)
        return false;
    if (obj.length === 0)
        return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object")
        return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key))
            return false;
    }

    return true;
}

function showLoader() {
    document.getElementById("loaderModal").style.display = "block";
}

function hideLoader() {
    document.getElementById("loaderModal").style.display = "none";
}

function closeDialog(id) {
    document.getElementById(id).style.display = "none";
    $('.defaultFocusButton').focus();
}

function todayDate() {
    var m_names = new Array("Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec");

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();
    return curr_date + "-" + m_names[curr_month] + "-" + curr_year;
}

