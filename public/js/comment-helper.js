
function addCommentTypeOptions() {

    $('#comment_type_options').append($('<option>', {
        value: "",
        text: 'select a type'
    }));

    $('#comment_type_options').append($('<option>', {
        value: 'D24h',
        text: 'D-24h'
    }));

    $('#comment_type_options').append($('<option>', {
        value: 'D90m',
        text: 'D-90m'
    }));
    $('#comment_type_options').append($('<option>', {
        value: 'D60m',
        text: 'D-60m'
    }));
}

function getFlightComments(data) {

    showLoader();
    $.removeData(document.body,'comments');
    $('#fireBtn').prop("disabled",true);
    $.ajax({
        type: 'POST',
        url: '/flight-comment/',
        data: data
    }).done(function (responseText, statusText) {
        hideLoader();
        $.data(document.body,'comments', responseText);
        generateFlightCommentsContent(responseText)


    }).fail(function (error, dataObj, xhr) {
        showErrorDialog("Get Flight comments failed.");
        hideLoader();
    });

}


function generateFlightCommentsContent(responseObject){
    $('#flightCommentsListBox').show();
    var comments = responseObject.flightComments;
    if (isEmpty(comments)) {

        showWarningDialog("No Flight Comments found for the flight selected");
        $('#flightListCommentsTable tbody').html("<tr><td colspan='4'>No Flight Comments found</td></tr>");

    } else {
        $('#fireBtn').prop("disabled",false);
        generateCommentsListHtml(comments);
    }
}

function generateCommentsListHtml(comments) {

    var html = null;
    $.each(comments, function (index, comment) {

        html += '<tr><td class="fit">' + (index + 1) + '</td><td class="fit">' +
            comment + '</td>';
        html += '<p>' + comment + '</p></br>';
    });
    $('#flightListCommentsTable tbody').html(html);


}

function triggerFlightComment(obj) {
    showLoader();
    var data = $.data(document.body,'comments');
    $.ajax({
        type: 'POST',
        url: '/flight-comment/fire',
        data: data
    }).done(function (responseText, statusText) {

        showSuccessDialog("Flight Comments have been pushed successfully!");
        //location.reload();
        $('#flightListBox').hide();
        hideLoader();
        $.removeData(document.body,'comments');

    }).fail(function (error, dataObj, xhr) {
        showErrorDialog("Push Flight comments failed. Error: " + JSON.stringify(error));
        hideLoader();
    });


}


$(document).ready(function () {
    preFillDateForm();
    addCommentTypeOptions();

    $('#flightInfoForm').submit(function () {

        $('#flightNumberError').addClass('hidden');
        $('#scheduledFlightDateError').addClass('hidden');
        $('#originError').addClass('hidden');
        $('#commentTypeError').addClass('hidden');
        var isError = false;
        if (isEmpty($('#operationCarrier').val())) {
            $('#flightNumberError').removeClass('hidden');
            isError = true;
        }
        if (isEmpty($('#flightNumber').val())) {
            $('#flightNumberError').removeClass('hidden');
            isError = true;
        }
        if (isEmpty($('#scheduledFlightDate').val())) {
            $('#scheduledFlightDateError').removeClass('hidden');
            isError = true;
        }
        if (isEmpty($('#origin').val())) {
            $('#originError').removeClass('hidden');
            isError = true;
        }
        if (isEmpty($('#destination').val())) {
            $('#originError').removeClass('hidden');
            isError = true;
        }
        if (isEmpty($('#comment_type_options').val())) {
            $('#commentTypeError').removeClass('hidden');
            isError = true;
        }
        if (!isError) {

            var flight = {
                'operationCarrier': $('#operationCarrier').val().toUpperCase(),
                'flightNumber': $('#flightNumber').val(),
                'scheduledFlightDate': $('#scheduledFlightDate').val(),
                'origin': $('#origin').val().toUpperCase(),
                'destination': $('#destination').val().toUpperCase()
            };
            var data = {
                'flight': flight,
                'key': $('#comment_type_options').val()
            };

            getFlightComments(data);
        }

        return false;
    });
});

function logoutFromComments() {

    $.get('/auth/logout', function(){
        window.location = '/auth/login'
    });

}
function showErrorDialog(msg) {
    $('#popupErrorText').text(msg);
    $("#errorModal").css("display","block");
    $("#errorPopUpButton").focus();
}

function showWarningDialog(msg) {
    $('#popupWarningText').text(msg);
    $("#warningModal").css("display","block");
    $("#warningPopUpButton").focus();
}
function showSuccessDialog(msg) {
    $('#popupSuccessText').text(msg);
    $("#doneModal").css("display","block");
    $("#donePopUpButton").focus();
}



function closeCommentDoneDialog(id) {
    document.getElementById(id).style.display = "none";
    $('.defaultFocusButton').focus();
    location.reload();
}