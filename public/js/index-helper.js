function preFillSearchBox() {

    $('#datepicker input').datepicker({
        format: 'dd-M-yyyy',
        startDate: '-2d',
        endDate: '+3d',
        autoclose: true,
        todayHighlight: true,
        container: '#datepicker',
        allowInputToggle: true,
        keyboardNavigation: true
    });
    var flight = getQueryParamsToObject();
    if (flight != null) {
        $('#origin').val(flight.origin);
        $('#destination').val(flight.destination);
        $('#datepicker input').datepicker('update', flight.argsFlight);
    } else {
        $('#datepicker input').datepicker('update', todayDate());
    }
}

function generateFlightList(responseObject, isFromWeb) {
    if (responseObject.redirect) {
        var flight = responseObject.flights;
        delete flight["timings"];
        redirectCommitPageURL(flight, isFromWeb);
    } else {
        $('#flightListBox').show();
        if (isEmpty(responseObject.flights)) {
            showWarningDialog("No Jump Seat requests found for the flight(s) selected");
            $('#flightListTable tbody').html("<tr><td colspan='4'>No Jump Seat requests found for the flight(s) selected</td></tr>");
        } else {
            generateFlightListHtml(responseObject.flights);
        }
    }
    hideLoader();
    return false;
}

function redirectCommitPageURL(object, isFromWeb) {
    var url = '/seat-allocation/admin/commit';
    if (isFromWeb) {
        url += '/web';
    }
    url += '?';
    window.location.href = url + $.param(object);
}

function generateFlightListHtml(flights) {
    var html = null;
    $.each(flights, function (key, flight) {
        html += '<tr><td class="fit">' + flight.operationCarrier + flight.flightNumber + '</td><td class="fit">' +
            flight.origin + ' - ' + flight.destination + '</td><td class="fit">' +
            flight.timings + '</td><td class="fit"><p class="text-center" style="margin : auto;padding:2px;"><button type="button" data-operationCarrier="' + flight.operationCarrier + '" data-flightNumber="' + flight.flightNumber + '"' +
            'data-origin="' + flight.origin + '" data-destination="' + flight.destination + '"' + 'data-scheduledFlightDate="' + flight.scheduledFlightDate + '"' + 'data-argsFlight="' + flight.argsFlight + '"' +
            ' class="btn btn-default commitPageButton" onclick="redirectToCommitPage($(this), isFromWeb);">Select</button></p></td>';
    });
    $('#flightListTable tbody').html(html);
    // var html = null;
    // $.each(flights, function (key, flight) {
    //     html += '<tr><td class="fit">' + flight.carrierCode + flight.flightNumber + '</td><td class="fit">' +
    //         flight.origin + ' - ' + flight.destination + '</td><td class="fit">' +
    //         flight.timings + '</td><td class="fit"><p class="text-center" style="margin : auto;padding:2px;"><button type="button" data-operationCarrier="' + flight.carrierCode + '" data-flightNumber="' + flight.flightNumber + '"' +
    //         'data-origin="' + flight.origin + '" data-destination="' + flight.destination + '"' + 'data-scheduledFlightDate="' + flight.scheduledFlightDate + '"' + 'data-argsFlight="' + flight.argsFlight + '"' +
    //         ' class="btn btn-default commitPageButton" onclick="redirectToCommitPage($(this), isFromWeb);">Select</button></p></td>';
    // });
    //
    // $('#flightListTable tbody').html(html);
    // $(".commitPageButton:first").focus();
}

function redirectToCommitPage(button, isFromWeb) {
    var flight = button.data();
    var object = {
        operationCarrier: flight.operationcarrier,
        flightNumber: flight.flightnumber,
        origin: flight.origin,
        destination: flight.destination,
        argsFlight: flight.argsflight,
        scheduledFlightDate: flight.scheduledflightdate
    };
    // redirectCommitPageURL(object, isFromWeb);
    alert(JSON.stringify(object))
}

function searchFlights(form, isFromWeb) {
    showLoader();
    $.ajax({
        type: 'GET',
        url: '/company/flights?' + $(form).serialize(),
        success: function (responseText, statusText) {
            generateFlightList(responseText, isFromWeb);
        },
        error: function (error, dataObj, xhr) {
            hideLoader();
            showWarningDialog(error.responseJSON.errCode + ' : ' + error.responseJSON.errDesc);
        }
    });
    return false;
}
