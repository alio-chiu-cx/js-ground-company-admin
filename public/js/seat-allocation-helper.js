var isError = false;
var requestObject = null;
var refreshPageInterval = null;
var allowCommitObject = {};

function refreshData() {
    $('#commitSeatAllocation').focus();
    refreshPage();
}

function commitSeat() {
    if (allowCommitObject.allowCommit == true) {
        var list = document.getElementById('commitSeatType');
        requestObject.checkInStaff = $('#CMUsername').val();
        window.clearInterval(refreshPageInterval);
        var body = {
            data: requestObject,
            commitType: list[list.selectedIndex].value
        };
        showLoader();
        $.ajax({
            type: "POST",
            url: "/seat-allocation/commit",
            contentType: "application/json",
            data: JSON.stringify(body),
            success: function (responseText, statusText) {
                hideLoader();
                if (!isEmpty(responseText)) {
                    generateCommitSeatStatusDialog(responseText);
                } else {
                    refreshPage();
                }
            },
            error: function (error, dataObj, xhr) {
                var data = error.responseJSON;
                hideLoader();
                if (!isEmpty(data) && !isEmpty(data.errDesc)) {
                    showErrorDialog(data.errDesc);
                    generateEmptyHtml(data.errDesc);
                } else {
                    showErrorDialog("Unknowwn Error Occured, Please contact support.");
                    generateEmptyHtml("Unknowwn Error Occured, Please contact support.");
                }
            }
        });
    } else {
        showErrorDialog(allowCommitObject.allowCommitErrorMsg);
        hideLoader();
    }
}

function generateCommitSeatStatusDialog(seatAllocationStatusObject) {
    if (!isEmpty(seatAllocationStatusObject)) {
        var html = '<div style="padding : 12px;"><table class="jsms-table">' +
                '<tr>' +
                '<th colspan="3">Commit Jump Seat Status</th>' +
                '</tr>' +
                '<tr>' +
                '<th class="fit">Customer</th>' +
                '<th class="fit">Allocated Seat</th>' +
                '<th class="fit">Error/Warning</th>' +
                '</tr>';

        seatAllocationStatusObject.forEach(function (seatAllocationObject)
        {
            var forInfoMessage = !isEmpty(seatAllocationObject.jsmsMessage) ? '(' + seatAllocationObject.jsmsMessage + ')' : '';
            html += '<tr><td class="fit">' + seatAllocationObject.paxName + '</td>' +
                    '<td class="fit">' + seatAllocationObject.allocatedSeat + '</td>' +
                    '<td class="fit">' + seatAllocationObject.soapErrorMessage + forInfoMessage + '</td>' +
                    '</tr>';
        });

        html = html + '</table><br/><div class="text-center"><input type="submit" class="btn btn-info" value="OK" id="okDailogButton" onclick="closeStatusDialog();" autofocus/></div></div>';
        $('#allocationStatusDiv').html(html);
        $("#openModal").css('display', 'block');
        $('#okDailogButton').focus();

    }
}

function closeStatusDialog() {
    $('#commitSeatAllocation').focus();
    $("#openModal").css('display', 'none');
    refreshData();
}

function init() {
    var refreshPageIntervalTime = $("#refreshIntervalTime").val();
    var _this = this;
    refreshPageInterval = setInterval(function () {
        refreshData();
    }, _this.isEmpty(refreshPageIntervalTime) ? 30000 : refreshPageIntervalTime);
}

function getRecommandationList(flightDetails) {
    var url = '/seat-allocation?flightNumber=' + flightDetails.flightNumber + '&operationCarrier=' + flightDetails.operationCarrier + '&scheduledFlightDate=' + flightDetails.scheduledFlightDate + '&origin=' + flightDetails.origin;
    window.clearInterval(refreshPageInterval);
    showLoader();
    $.ajax({
        type: 'GET',
        url: url,
        success: function (responseText, statusText) {
            hideLoader();
            if (!isEmpty(responseText)) {
                generateSeatAllocationData(responseText);
                init();
            } else {
                generateEmptyHtml("No Jump Seat Request found for selected flight.");
            }
        },
        error: function (error, dataObj, xhr) {
            var data = error.responseJSON;
            hideLoader();
            if (!isEmpty(data) && !isEmpty(data.errDesc)) {
                showErrorDialog(data.errDesc);
                generateEmptyHtml(data.errDesc);
            } else {
                showErrorDialog("Unknowwn Error Occured, Please contact support.");
                generateEmptyHtml("Unknowwn Error Occured, Please contact support.");
            }
        }
    });
}

function generateEmptyHtml(message) {
    window.clearInterval(refreshPageInterval);
    $('#container').addClass('hidden');
    $('#errorText').text(message);
    $('#errorContainer').removeClass('hidden');
}

function generateSeatAllocationData(data) {
    requestObject = data;
    if (!isEmpty(data) && !isEmpty(data.flightDetails)) {
        $('#flightNumberText').html(data.flightDetails.operationCarrier + data.flightDetails.flightNumber);
        $('#stdDetails').html("STD " + data.flightDetails.scheduleDepartureTime);
        $('#acceptance').html("Acceptance " + data.acceptance);
        $('#originDestDetails').html(data.flightDetails.scheduleDepartureDate + " " + data.flightDetails.origin + " => " + data.flightDetails.destination);
        allowCommitObject = {
            allowCommit: data.allowCommit,
            allowCommitCode: data.allowCommitCode,
            allowCommitErrorMsg: data.allowCommitErrorMsg
        };

        generateSeatAvailabilityTable(data);
        generateFlightDeckList(data.captainSeatPaxs);
        generateFlightCrewList(data.cabinCrewSeatPaxs);
        $('#errorContainer').addClass('hidden');
        $('#container').removeClass('hidden');
        $('#commitSeatAllocation').focus();
    }
}
function generateSeatAvailabilityTable(data) {
    var html = '<table class="jsms-table">' +
            '<tr>' +
            '<td class="fit"></td>' +
            '<td class="fit">Total Available</td>' +
            '<td class="fit">After Onload</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="fit">Flight Deck Seat</td>' +
            '<td class="fit">' + data.beforeOnload.fd + '</td>' +
            '<td class="fit">' + data.availableSeats.fd + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="fit">VFCR Seat</td>' +
            '<td class="fit">' + data.beforeOnload.vfcr + '</td>' +
            '<td class="fit">' + data.availableSeats.vfcr + '</td>' +
            '</tr>' +
            '<tr>' +
            '<td class="fit">Cabin Crew Seat</td>' +
            '<td class="fit">' + data.beforeOnload.cc + '</td>' +
            '<td class="fit">' + data.availableSeats.cc + '</td>' +
            '</tr>' +
            '</table>';

    $('#seatAvailabilityTable').html(html);
}

function generateFlightDeckList(flightCrewSeats) {
    var html = '<table class="jsms-table">' +
            '<tr>' +
            '<th class="fit">Customer</th>' +
            '<th class="fit">Commander Priority</th>' +
            '<th class="fit">Safety Qualified</th>' +
            '<th class="fit">Recommendation</th>' +
            '<th class="fit">Allocated Seat</th>' +
            '<th class="fit">Status</th>' +
            '<th class="fit">Ticket Priority</th>' +
            '<th class="fit">DOJ</th>' +
            '<th class="fit">Security Number</th>' +
            '</tr>';
    if (!isEmpty(flightCrewSeats)) {
        flightCrewSeats.forEach(function (flightCrewSeat)
        {
            html += '<tr><td class="fit">' + flightCrewSeat.paxName + '</td><td class="fit">' + flightCrewSeat.priority + '</td><td class="fit">' + flightCrewSeat.isSafetyQualified + '</td>' +
                    '<td class="fit">' + (flightCrewSeat.status === "SBY" ? 'Onload ' + flightCrewSeat.recommendation : flightCrewSeat.recommendation) + '</td>' +
                    '<td class="fit">' + flightCrewSeat.seatNumber + '</td>' +
                    '<td class="fit">' + flightCrewSeat.status + '</td>' +
                    '<td class="fit">' + flightCrewSeat.ticketPriority + '</td>' +
                    '<td class="fit">' + flightCrewSeat.doj + '</td>' +
                    '<td class="fit">' + flightCrewSeat.securityNumber + '</td>' +
                    '</tr>';
        });
    } else {
        html += '<tr><td colspan="9">No Jump Seat Request Found</td></tr>';
    }

    html = html + '</table>';
    $('#flightCrewList').html(html);
}

function generateFlightCrewList(flightDeckSeats) {
    var html = '<table class="jsms-table">' +
            '<tr>' +
            '<th class="fit">Customer</th>' +
            '<th class="fit">Safety Qualified</th>' +
            '<th class="fit">Recommendation</th>' +
            '<th class="fit">Allocated Seat</th>' +
            '<th class="fit">Status</th>' +
            '<th class="fit">Ticket Priority</th>' +
            '<th class="fit">DOJ</th>' +
            '<th class="fit">Security Number</th>' +
            '</tr>';
    if (!isEmpty(flightDeckSeats)) {
        flightDeckSeats.forEach(function (flightDeckSeat)
        {
            html += '<tr><td class="fit">' + flightDeckSeat.paxName + '</td>' +
                    '<td class="fit">' + flightDeckSeat.isSafetyQualified + '</td>' +
                    '<td class="fit">' + flightDeckSeat.recommendation + '</td>' +
                    '<td class="fit">' + (flightDeckSeat.seatNumber === null ? "" : flightDeckSeat.seatNumber) + '</td>' +
                    '<td class="fit">' + flightDeckSeat.status + '</td>' +
                    '<td class="fit">' + flightDeckSeat.ticketPriority + '</td>' +
                    '<td class="fit">' + flightDeckSeat.doj + '</td>' +
                    '<td class="fit">' + flightDeckSeat.securityNumber + '</td>' +
                    '</tr>';
        });
    } else {
        html += '<tr><td colspan="8">No Jump Seat Request Found</td></tr>';
    }

    html = html + '</table>';
    $('#flightDeckList').html(html);
}

function refreshPage() {
    var flightDetails = getQueryParamsToObject();
    getRecommandationList(flightDetails);
    return isError;

}

function validateUserPort(boardingPoint) {
    var location = getFullLocation();
    location = location['env.fullLocation'];
    var locationArray = location.split("/");
    if (boardingPoint === locationArray[1]) {
        return true;
    }
    return false;
}

/*
 * WILL BE USED AFTER 1A FIX
 */

//function getFlightDetails() {
//    var flightInfo = getFlight();
//    var flightArray = flightInfo.split("/");
//    if (isEmpty(flightArray)) {
//        isError = true;
//        generateEmptyHtml("No Flight Details Found from CM!!");
//        return {
//            error: "No Flight Details Found from CM!!"
//        };
//    } else {
//        if (validateUserPort(flightArray[3])) {
//            var flightNumber = flightArray[1];
//            flightNumber = flightNumber.substring(0, 1) === 0 || flightNumber.substring(0, 1) === "0" ? flightNumber.substring(1, 4) : flightNumber;
//            return {
//                operationCarrier: flightArray[0].trim(),
//                flightNumber: flightNumber,
//                scheduledFlightDate: flightArray[2].trim(),
//                origin: flightArray[3].trim()
//            };
//            isError = false;
//        } else {
//            isError = true;
//            generateEmptyHtml("Boarding Point and Work Location are different!!");
//            return {
//                error: "Boarding Point and Work Location are different!!"
//            };
//        }
//
//    }
//}

function openflight(isFromWeb) {
    var url = "/seat-allocation/admin/";
    if (isFromWeb) {
        url += 'web/';
    }
    url += location.search;
    
    window.location.href = url;
}
