/**
 * Created by simeon on 18/11/2016.
 */

"use strict";

const GROUND_STAFF_GROUP_NAME = "JSMS_AirportStaff";
const GROUND_STAFF_ADMIN_GROUP_NAME = "JSMS_Admin";
const SA_CI_ACCECPTED = "CAC";
const SA_CI_NOT_REGISTERED = "CNA";
const SA_CI_NOT_TRAVELLING = "CRJ";
const SA_CI_STANDBY = "CST";
const SA_CI_JMP = "JMP";
const SA_CI_CRW = "CRW";
const SEAT_CM_JMP = "JP-JMP";
const SEAT_CM_CRW = "JP-CRW";
const JSMS_CC = "CAB";
const JSMS_FD = "FDK";
const JSMS_VFCR = "FCR";
const COMMIT_CUTOFF_TIME = 180;
const COMMIT_CUTOFF_CODE = "D3H";
const COMMIT_NOSEAT_CODE = "NOSEAT";
const COMMIT_NOREC_CODE = "NOREC";
const UNKNOWN_ERROR_MESSAGE = "Unknown Error Occured.";

const CHECK_IN_ACCEPTANCE_STATUS = {
    OP : "Open",
    CL : "Closed",
    FI : "Finalised",
    NO : "Not Open",
    SPD : "Suspended"
};

const NO_COMMIT_BEFORE_CUTOFF_TIME = "Commit Jump Seat is not allowed before 3 Hours of Departure";
const COMMIT_NO_SEAT_AVAILABLE = "Seat Allocation/ Commit Jump Seat is not allowed because no seat available";
const COMMIT_NO_REC_REQ = "No Jump Seat Request with Recommendation found.";

const SA_CI_STATUS = {
    CAC: "Accepted",
    CNA: "Not Yet Registered",
    CRJ: "Not Travelling",
    CST: "Standby",
    JMP: "Accepted (jump seat)",
    CRW: "Accepted (crew seat)"
};

const PASSENGER_TYPE = {
    "ADT": "A",
    "CHD": "C",
    "INF": "I",
    "IFT": "I",
    "INS": "767",
    "YTH": "A",
    "MIL": "A",
    "GOV": "A",
    "SRC": "A"
};
const SA_CI_NO_PNR = "No PNR";
const DEFAULT_PASSENGER_TYPE = "A";
const SA_CI_NO_PNR_CODE = "NO_PNR";

const IGNORE_CACHE = "ignoreCache";
//ERROR HANDLING
const ERR_SA0002 = "SA0002";
const ERR_SA0004 = "SA0004";
const ERR_SA0005 = "SA0005";
const ERR_SA0005_DESC = "Unexpected Error Occured, Please contact support";
const ERR_SA0003 = "SA0003";
const ERR_SA0002_DESC = "No Valid Jump Seat Request Found to allocate jump seat, Please check all staff must be on standby before commit seat.";
const ERR_SA0001 = "SA0001";
const ERR_SA0001_DESC = "No Jump Seat Request Found to allocate jump seat.";

module.exports = {
    GROUND_STAFF_GROUP_NAME, GROUND_STAFF_ADMIN_GROUP_NAME,
    SA_CI_ACCECPTED, SA_CI_NOT_REGISTERED, SA_CI_NOT_TRAVELLING, SA_CI_STANDBY, SA_CI_JMP, SA_CI_CRW,
    SEAT_CM_JMP, SEAT_CM_CRW, JSMS_CC, JSMS_FD, JSMS_VFCR,
    SA_CI_STATUS, SA_CI_NO_PNR, SA_CI_NO_PNR_CODE, PASSENGER_TYPE, DEFAULT_PASSENGER_TYPE,
    ERR_SA0003, ERR_SA0002, ERR_SA0001, ERR_SA0002_DESC, ERR_SA0001_DESC,
    COMMIT_CUTOFF_TIME, NO_COMMIT_BEFORE_CUTOFF_TIME, COMMIT_NO_SEAT_AVAILABLE,
    COMMIT_CUTOFF_CODE, COMMIT_NOSEAT_CODE, COMMIT_NOREC_CODE, COMMIT_NO_REC_REQ,
    UNKNOWN_ERROR_MESSAGE, ERR_SA0004, CHECK_IN_ACCEPTANCE_STATUS, IGNORE_CACHE, ERR_SA0005, ERR_SA0005_DESC
};
