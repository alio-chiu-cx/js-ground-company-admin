"use strict";

var express = require('express');
let moment = require('moment');
var appUtils = require('../utils/app-utils');
var loggerUtils = require('../utils/logger-utils');
var companyAdminHelper = require('./company-admin-helper');
const zipkinUtils = require('../utils/zipkin-utils');
var webUtils = require('../utils/web-utils');

var router = express.Router();



/**
 * @api {get} /seat-allocation/flights Get Flights With Jump Seat
 * @apiName GetFlightData
 * @apiGroup Seat Allocation
 *
 *
 * @apiParam {String} flightNumber Flight Number.
 * @apiParam {String} origin Flight Number.
 * @apiParam {String} destination Flight Number.
 * @apiParam {String} scheduledFlightDate Scheduled flight date of that flight.
 * @apiParam {String} operationCarrier Operative Carrier of Flight.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
 *   "redirect": false,
 *   "flights": [{
 *           "_id": {
 *               "operationCarrier": "CX",
 *               "flightNumber": "905",
 *               "origin": "HKG",
 *               "destination": "MNL",
 *               "scheduledFlightDate": "29Mar2017",
 *               "timings": "1555 - 0405",
 *               "argsFlight": "29-Mar-2017"
 *           }
 *       }]
 * }
 *
 *
 * @apiError (Error 4xx) RequiredFieldEmptyError RequiredFieldEmpty error.
 *
 * @apiErrorExample {json} RequiredFieldEmptyError-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "errCode": 900,
 *       "errDesc": "Missing input args."
 *     }
 *
 *
 * @apiError (Error 5xx) UnkownError Unkown error.
 *
 * @apiErrorExample {json} UnkownError-Response:
 *     HTTP/1.1 500 Bad Request
 *     {
 *       "errCode": 999,
 *       "errDesc": "Unkown error occurs."
 *     }
 *
 */
router.get('/flights', function (req, res) {
    loggerUtils.getCompanyAdminLogger().info("[searchFlight] company hit flights");

    zipkinUtils.recordApiInfo('api.company-admin.get-flights');

    let scheduledFlightDate = req.query.scheduledFlightDate;
    let origin = req.query.origin;
    let destination = req.query.destination;

    if (appUtils.isEmpty(scheduledFlightDate) || appUtils.isEmpty(origin)) {
        webUtils.responseWithInvalidParaErr(res);
        return;
    }

    let queryParams = {
        scheduledFlightDate: moment(appUtils.parseStringToDate(scheduledFlightDate)).format("YYYY-MM-DD"),
        origin: origin,
        destination : destination
    };
    companyAdminHelper.getFlights(queryParams).then(function (result) {
        res.json(result);
    }).catch(function (err) {
        loggerUtils.getCompanyAdminLogger().error("Error While Generating Flight List", err);
        webUtils.responseWithErrObj(res, webUtils.HTTP_SERVER_ERR_CODE, err);
    });
});


module.exports = router;
