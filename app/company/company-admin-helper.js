/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

"use strict";
let _ = require("underscore");
let moment = require('moment');
const constant = require('../constant/constant');
var envUtils = require('../utils/env-utils');
// var fabricxUtils = require('../utils/fabricx-utils');
var {zipkinRp} = require('../utils/zipkin-utils');
module.exports = {
    getFlights: function (queryParam) {
        return zipkinRp({
            uri: envUtils.getMicroServiceUrl(envUtils.API_URL) + "/flights/orig-dest-daterange",
            qs: {
                origin: queryParam.origin,
                destination: queryParam.destination,
                startDate: queryParam.scheduledFlightDate,
                endDate: queryParam.scheduledFlightDate,
                isCompanyAdmin: true
            },
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'backend-version': '1.1.2',
                [constant.IGNORE_CACHE]: (queryParam.ignoreCache) ? 'true' : 'false'
            },
            json: true
        }, {serviceName: 'search-flights'}).then(function (flights) {
            _.each(flights, function (flight) {
                var departDate = moment.utc(flight.flightDepartureTime).format("YYYY-MM-DD");
                var arrivalDate = moment.utc(flight.flightArrivalTime).format("YYYY-MM-DD");
                var days = moment.utc(arrivalDate).diff(moment.utc(departDate), 'days');
                flight.timings = moment.utc(flight.flightDepartureTime).format("HHmm") + " - " + moment.utc(flight.flightArrivalTime).format("HHmm") + (days > 0 ? '+' + days : (days < 0 ? '-' + days : ''));
                flight.argsFlight = moment.utc(flight.scheduledFlightDate, "YYYY-MM-DD").format("DD-MMM-YYYY");
                flight.scheduledFlightDate = moment.utc(flight.scheduledFlightDate, "YYYY-MM-DD").format("DDMMMYYYY");
                flight.origin = queryParam.origin;
                flight.destination = queryParam.destination;
                flight.scheduledFlightDate = queryParam.scheduledFlightDate;
            });
            let resObject = {
                redirect: false,
                flights: flights
            };
            return Promise.resolve(resObject);
        }).catch(function (err) {
            if (err.statusCode == 500 && err.error && err.error.errCode) {
                return Promise.reject(err.error);
            } else {
                return Promise.reject(err);
            }
        });
    }
}
;




