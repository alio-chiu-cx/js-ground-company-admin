"use strict";

var express = require('express');
var config = require('config');
var router = express.Router();
// let {verifyWebAccess} = require('../utils/express-middleware');

router.get('/', function (req, res) {
    res.render('index', {fromWeb: false});
});

router.get('/web', function (req, res) {
    res.render('index', {fromWeb: true});
});


module.exports = router;
