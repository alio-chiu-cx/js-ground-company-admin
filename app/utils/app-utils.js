/**
 * Created by siddharth.saraf on 8/9/16.
 */
"use strict";

var hasOwnProperty = Object.prototype.hasOwnProperty;
module.exports = {
    checkArrayContainsObject: function (objectArray, object) {
        if (objectArray.indexOf(object) > -1) {
            return true;
        }
        return false;
    },

    parseStringToDate: function (stringDate) {
        if (stringDate !== null || stringDate !== 'undefined') {
            return Date.parse(stringDate);
        }
        return null;
    },

    isEmpty: function (obj) {

        // null and undefined are "empty"
        if (obj == null)
            return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (obj.length > 0)
            return false;
        if (obj.length === 0)
            return true;

        // If it isn't an object at this point
        // it is empty, but it can't be anything *but* empty
        // Is it empty?  Depends on your application.
        if (typeof obj !== "object")
            return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in obj) {
            if (hasOwnProperty.call(obj, key))
                return false;
        }

        return true;
    },
    getErrorCodeDescription : function(errorCode){
        if(errorCode === "ENOTFOUND"){
            return "Error while connectiong Host, not reachable.";
        }else{
            return "Unknown Error!";
        }
    },
    generateSOAPErrorInfo: function (error, defaultErrorCode, defaultErrorMsg) {
        let _this = this;
        if (!_this.isEmpty(error.code)) {
            let cause = error.code;
            return {
                errCode : cause,
                errDesc : _this.getErrorCodeDescription(cause)
            };
        } else if (!_this.isEmpty(error.Code)) {
            return {
                errCode : error.Code,
                errDesc : error.Text
            };
        } else if (!_this.isEmpty(error.root)) {
            return {
                errCode : error.root.Envelope.Body.Fault.faultcode,
                errDesc : error.root.Envelope.Body.Fault.faultstring
            };
        } else {
            return {
                errCode : defaultErrorCode,
                errDesc : defaultErrorMsg
            };
        }
    },
    
    nullCheckReturn : function(obj){
        return this.isEmpty(obj) ? null : obj;
    },
    
    toUpperAndMatch : function(a,b){
        a = !this.isEmpty(a) ? a.toUpperCase() : null;
        b = !this.isEmpty(b) ? b.toUpperCase() : null;
        return a === b;
    }
};
