"use strict";

var rp = require('request-promise');
const {HttpHeaders, Annotation} = require('zipkin');
var Promise = require("bluebird");
var constant = require('../constant/constant');

let _tracer = null;

function setTracer(theTracer) {
    _tracer = theTracer;
}

function getHeaders(traceId, opts) {
    const headers = opts.headers || {};
    headers[HttpHeaders.TraceId] = traceId.traceId;
    headers[HttpHeaders.SpanId] = traceId.spanId;

    traceId._parentId.ifPresent(psid => {
        headers[HttpHeaders.ParentSpanId] = psid;
    });
    traceId.sampled.ifPresent(sampled => {
        headers[HttpHeaders.Sampled] = sampled ? '1' : '0';
    });

    return headers;
}

function zipkinRp(opts, {serviceName = constant.SERVICE_UNKNOWN, remoteServiceName}) {
    return new Promise((resolve, reject) => {
        _tracer.scoped(() => {
            _tracer.setId(_tracer.createChildId());
            const traceId = _tracer.id;

            const method = opts.method || 'GET';
            _tracer.recordServiceName(serviceName);
            _tracer.recordRpc(method.toUpperCase());
            _tracer.recordBinary('http.uri', opts.uri);
            if (opts.qs) {
                _tracer.recordBinary('http.qs', JSON.stringify(opts.qs));
            }
            if (opts.body) {
                _tracer.recordBinary('http.body', JSON.stringify(opts.body));
            }
            _tracer.recordAnnotation(new Annotation.ClientSend());
            if (remoteServiceName) {
                _tracer.recordAnnotation(new Annotation.ServerAddr({
                    serviceName: remoteServiceName
                }));
            }

            const headers = getHeaders(traceId, opts);
            const zipkinOpts = Object.assign({}, opts, {headers});

            rp(zipkinOpts).promise().bind(this).then(res => {
                _tracer.scoped(() => {
                    _tracer.setId(traceId);
                    _tracer.recordAnnotation(new Annotation.ClientRecv());
                });
                resolve(res);
            }).catch(err => {
                _tracer.scoped(() => {
                    _tracer.setId(traceId);
                    _tracer.recordBinary('request.error', err.toString());
                    _tracer.recordAnnotation(new Annotation.ClientRecv());
                });
                reject(err);
            });
        });
    });
}

function recordApiInfo(apiName) {
    _tracer.scoped(() => {
        _tracer.recordServiceName(apiName);
    });
}

module.exports = {zipkinRp, setTracer, recordApiInfo};
