"use strict";

const INVALID_PARA_ERR_CODE = 'CJ-001';
const UNAUTHORIZED_ACCESS_ERR_CODE = 'CJ-002';
const GENERAL_ERR_CODE = 'CJ-GENERAL';

let errInfo = {
    [INVALID_PARA_ERR_CODE]: 'Input parameter is missing or invalid',
    [UNAUTHORIZED_ACCESS_ERR_CODE]: 'Unauthorized access',
    [GENERAL_ERR_CODE]: 'Unknown error'
};

function getErrDesc(errCode) {
    return (errInfo.hasOwnProperty(errCode)) ? errInfo[errCode] : errInfo[GENERAL_ERR_CODE];
}

class JsError extends Error {
    constructor(errCode, errDesc = "") {
        super();
        this.name = "JsError";

        if (!errCode) {
            errCode = GENERAL_ERR_CODE;
        }
        this.errCode = errCode;
        this.errDesc = errDesc;

        Error.captureStackTrace(this, JsError);
    }

    valueOf() {
        return super.valueOf();
    }

    toString() {
        return this.stack;
    }
}

module.exports = {getErrDesc, JsError, INVALID_PARA_ERR_CODE, GENERAL_ERR_CODE,
    UNAUTHORIZED_ACCESS_ERR_CODE};