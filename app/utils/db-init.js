"use strict";

const config = require('config');
const util = require('util');
var mongoose = require('mongoose');
var loggerUtils = require('../utils/logger-utils');

mongoose.Promise = require('bluebird');

var db = mongoose.connection;

db.on('error', function(err) {
    loggerUtils.getDBLogger().error("DB Connection error!", err);
});

db.on('disconnected', function() {
    loggerUtils.getDBLogger().info("Mongoose default connection disconnected");
});

db.on('reconnected', function() {
    loggerUtils.getDBLogger().info("Mongoose default connection reconnected");
});

db.on('open', function() {
    loggerUtils.getDBLogger().info("Mongoose default connection open");
});

process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        loggerUtils.getDBLogger().info("Mongoose default connection disconnected through app termination");
        process.exit(0);
    });
});

process.on("unhandledRejection", function(reason, promise) {
    loggerUtils.getUnhandledRejectionLogger().error(reason);
    throw reason;
});

exports.initDb = function() {
    var connectUri = util.format("%s/%s", config.get("db.connectUrl"), config.get("db.dbName"));
    return mongoose.connect(connectUri, {
        promiseLibrary: require('bluebird'),
        user: config.get("db.dbUsername"),
        pass: config.get("db.dbPassword"),
        server: {
            reconnectTries: Number.MAX_VALUE,
            socketOptions: {
                keepAlive: 1
            }
        },
    }).catch(function (err) {
        loggerUtils.getDBLogger().error("DB Connection error!", err);
        Promise.reject(err);
    });
};
