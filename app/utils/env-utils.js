"use strict";

var config = require('config');

const LDAP_URL = "ldapUrl";
const FABRICX_URL = "fabricxUrl";
const RULE_ENGINE_URL = "ruleEngineUrl";
const ZIPKIN_BASE_URL = "zipkinBaseUrl";
const API_URL = "apiUrl";

let serviceMap = {};

serviceMap[RULE_ENGINE_URL] = {
    envKey: "JS_RULE_ENGINE_URL",
    configKey: "microService.ruleEngineUrl"
};
serviceMap[LDAP_URL] = {
    envKey: "JS_LDAP_URL",
    configKey: "microService.ldapUrl"
};
serviceMap[FABRICX_URL] = {
    envKey: "JS_FABRICX_URL",
    configKey: "microService.fabricxUrl"
};
serviceMap[API_URL] = {
    envKey: "JS_API_URL",
    configKey: "microService.apiUrl"
};

serviceMap[ZIPKIN_BASE_URL] = {
    envKey: "JS_ZIPKIN_BASE_URL",
    configKey: "zipkinBaseUrl"
};

function getMicroServiceUrl(serviceKey) {
    let serviceInfo = serviceMap[serviceKey];
    if (serviceInfo == null) {
        return null;
    }

    let serviceUrl = process.env[serviceInfo.envKey];
    if (serviceUrl == null) {
        serviceUrl = config.get(serviceInfo.configKey);
    }

    return serviceUrl;
}

module.exports = {RULE_ENGINE_URL, LDAP_URL, FABRICX_URL, ZIPKIN_BASE_URL, API_URL, getMicroServiceUrl};
