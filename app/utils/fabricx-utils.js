"use strict";

var Promise = require("bluebird");
var envUtils = require('./env-utils');
var {zipkinRp} = require('./zipkin-utils');
const constant = require('../constant/constant');

function postSingleFlightComment(comment) {

    return zipkinRp({
        method: 'POST',
        uri: envUtils.getMicroServiceUrl(envUtils.FABRICX_URL) + "/soap/flight-comments/",
        body: comment,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        json: true
    }, {serviceName: 'flight-comments'}).then(function(result) {
        return Promise.resolve(result);
    }, function(err) {
        if (err.statusCode == 500 && err.error && err.error.errCode) {
            return Promise.reject(err.error);
        } else {
            return Promise.reject(err);
        }
    });
}


module.exports = {
    searchFlights: function (searchQs, ignoreCache = false) {
        console.log('fabric utils hit');
        return zipkinRp({
            uri: envUtils.getMicroServiceUrl(envUtils.FABRICX_URL) + "/soap/flights",
            qs: searchQs,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                [constant.IGNORE_CACHE]: (ignoreCache) ? 'true' : 'false'
            },
            json: true
        }, {serviceName: 'search-flights'}).catch(function (err) {
            if (err.statusCode == 500 && err.error && err.error.errCode) {
                return Promise.reject(err.error);
            } else {
                return Promise.reject(err);
            }
        });
    },
    getCheckedInFlightPassengers: function(searchQs) {
        return zipkinRp({
            uri: envUtils.getMicroServiceUrl(envUtils.FABRICX_URL) + "/soap/checkin/passengers",
            qs: searchQs,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            json: true
        }, {serviceName: 'checkin-passengers'}).then(function(result) {
            return Promise.resolve(result);
        }, function(err) {
            if (err.statusCode == 500 && err.error && err.error.errCode) {
                return Promise.reject(err.error);
            } else {
                return Promise.reject(err);
            }
        });
    },
    commitSeatAllocation: function(requestObj) {
        return zipkinRp({
            method: 'POST',
            uri: envUtils.getMicroServiceUrl(envUtils.FABRICX_URL) + "/soap/commit-seat",
            body: requestObj,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            json: true
        }, {serviceName: 'commit-seat'}).then(function(result) {
            return Promise.resolve(result);
        }, function(err) {
            if (err.statusCode == 500 && err.error && err.error.errCode) {
                return Promise.reject(err.error);
            } else {
                return Promise.reject(err);
            }
        });
    },
    /** flightCommentObj{
        *    "flightDetail":
        *   {
            *          "operationCarrier" : "KA",
            *          "flightNumber"     : "154",
            *          "origin"           : "HKG",
            *          "scheduledFlightDate" : "2017-01-19"
            *      },
        *    "flightComments" : "This is Test Comment"
        * }
     */
    postFlightComments: function(flightComments){

        let comments = flightComments.flightComments;
        let flight = flightComments.flightDetail;

        return Promise.each(comments, (singleComment)=>{
            let commentObj = {
                "flightDetail" : flight,
                "flightComments" : singleComment
            };
            return postSingleFlightComment(commentObj);
        });

    }
};