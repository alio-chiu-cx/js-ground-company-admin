"use strict";

const config = require('config');
const path = require('path');
var winston = require('winston');
var DailyRotateFile = require('winston-daily-rotate-file');
var moment = require('moment');
const fs = require('fs');

var logFolderPath = config.get("storagePath.logs");
try {
    fs.statSync(logFolderPath);
} catch(err) {
    if (err.code == "ENOENT") {
        fs.mkdirSync(logFolderPath);
    } else {
        throw err;
    }
}

const COMPANY_ADMIN_LOG_FILE_NAME = "company-admin";
const DB_LOG_NAME = "db";
const UNHANDLED_REJECTION_LOG_NAME = "unhandled-rejection";
const CLUSTER_LOG_NAME = "cluster";

let companyAdminLogger = null;
let dbLogger = null;
let unhandledRejectionLogger = null;
let clusterLogger = null;

function getLogger(logName) {
    var logger = new (winston.Logger)({
        transports: [
            new DailyRotateFile({
                level: 'info',
                json: false,
                timestamp: function() {
                    return moment().format('YYYY-MM-DD HH:mm:ss.SSSZZ');
                },
                filename: path.join(logFolderPath, logName),
                datePattern: '.yyyy-M-dd.log'
            })
        ]
    });

    return logger;
}

exports.getCompanyAdminLogger = function() {
    if (companyAdminLogger == null) {
        companyAdminLogger = getLogger(COMPANY_ADMIN_LOG_FILE_NAME);
    }

    return companyAdminLogger;
};

exports.getDBLogger = function() {
    if (dbLogger == null) {
        dbLogger = getLogger(DB_LOG_NAME);
    }

    return dbLogger;
};

exports.getUnhandledRejectionLogger = function() {
    if (unhandledRejectionLogger == null) {
        unhandledRejectionLogger = getLogger(UNHANDLED_REJECTION_LOG_NAME);
    }

    return unhandledRejectionLogger;
};

exports.getClusterLogger = function() {
    if (clusterLogger == null) {
        clusterLogger = getLogger(CLUSTER_LOG_NAME);
    }

    return clusterLogger;
};
