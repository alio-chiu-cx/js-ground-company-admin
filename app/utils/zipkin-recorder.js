"use strict";

const {getMicroServiceUrl, ZIPKIN_BASE_URL} = require('./env-utils');
const { BatchRecorder } = require('zipkin');
const { HttpLogger } = require('zipkin-transport-http');

const zipkinBaseUrl = getMicroServiceUrl(ZIPKIN_BASE_URL);
const recorder = new BatchRecorder({
    logger: new HttpLogger({
        endpoint: `${zipkinBaseUrl}/api/v1/spans`
    })
});

module.exports = recorder;
