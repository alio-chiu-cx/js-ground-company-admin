'use strict'

var {getErrDesc, JsError, GENERAL_ERR_CODE, INVALID_PARA_ERR_CODE} = require('./js-error');

const HTTP_CLIENT_ERR_CODE = 400;
const HTTP_UNAUTHORIZED_ERR_CODE = 401;
const HTTP_SERVER_ERR_CODE = 500;

const ERR_CODE_NAME = "errCode";
const ERR_DESC_NAME = "errDesc";


function responseWithErr(res, statusCode, errCode, errMsg) {
    res.status(statusCode).json({
        [ERR_CODE_NAME]: errCode ? errCode.toString() : "",
        [ERR_DESC_NAME]: errMsg
    });
}

function responseWithErrObj(res, statusCode, err) {
    let errCode = (err && err.errCode) ? err.errCode : GENERAL_ERR_CODE;
    let errDesc = getErrDesc(errCode);

    responseWithErr(res, statusCode, errCode, errDesc);
}

function responseWithInvalidParaErr(res) {
    let missingParaErr = new JsError(INVALID_PARA_ERR_CODE);
    responseWithErrObj(res, HTTP_CLIENT_ERR_CODE, missingParaErr);
}

module.exports = {HTTP_CLIENT_ERR_CODE, HTTP_SERVER_ERR_CODE, INVALID_PARA_ERR_CODE,
    HTTP_UNAUTHORIZED_ERR_CODE, responseWithErr, responseWithErrObj,
    responseWithInvalidParaErr};
