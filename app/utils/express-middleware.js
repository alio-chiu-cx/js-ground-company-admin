'use strict'

const config = require('config');
let _ = require('underscore');
let envUtils = require('../utils/env-utils');
const {zipkinRp} = require('../utils/zipkin-utils');

const MEMBER_OF_API = "/staff/memberof";
const USER_INFO_HEADER = "Remote-User";
const GALACXY_ID_RE = /(.+)@/;
const UNAUTHORIZED_MSG = 'You are not authorized to access this page.';

function responseUnauthorizedAccess(res) {
    // support to be 401 but proxy pass with had unexpected behaviour
    res.send(UNAUTHORIZED_MSG);
}

function verifyWebAccess(req, res, next) {
    let checkADGroups = config.get("checkADGroups");
    if (checkADGroups) {
        let userInfo = req.header(USER_INFO_HEADER);
        let found = userInfo ? userInfo.match(GALACXY_ID_RE) : null;
        if (found && found.length > 1) {
            let galacxyId = found[1];

            zipkinRp({
                method: 'GET',
                uri: envUtils.getMicroServiceUrl(envUtils.LDAP_URL) + MEMBER_OF_API,
                qs: {galacxyId},
                json: true
            }, {serviceName: 'staff-member-of'}).then((result) => {
                if (_.find(result.memberOf, (group) => {
                    return (_.find(checkADGroups, (adGroup) => {
                        return group.includes(adGroup);
                    }) != null);
                })) {
                    next();
                } else {
                    responseUnauthorizedAccess(res);
                }
            }).catch((err) => {
                responseUnauthorizedAccess(res);
            });
        } else {
            responseUnauthorizedAccess(res);
        }
    } else {
        next();
    }
}

module.exports = {verifyWebAccess};
