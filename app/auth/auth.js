"use strict";

let express = require('express');
let router = express.Router();
let passport = require("passport");
let LocalAuthStrategy = require("passport-local").Strategy;
let cons = require('../constant/constant');
const {zipkinRp} = require('../utils/zipkin-utils');
let envUtils = require('../utils/env-utils');

const config = require("config");
const GROUND_STAFF_API = "/ground-staff/auth";
const FAILED_MESSAGE_KEY = 'failedMessage';

passport.use('local', new LocalAuthStrategy(
        {passReqToCallback: true},
        function (req, username, password, done) {

            if (username && password) {
                if (username == config.get('superUser.name') &&
                        password == config.get('superUser.password')) {
                    let user = {
                        username: username,
                        password: password,
                        group: cons.GROUND_STAFF_ADMIN_GROUP_NAME
                    };
                    return done(null, user);
                } else {
                    zipkinRp({
                        method: 'POST',
                        url: envUtils.getMicroServiceUrl(envUtils.LDAP_URL) + GROUND_STAFF_API,
                        body: {
                            galacxyId: username,
                            password: password
                        },
                        json: true
                    }, {serviceName: 'ground-staff-auth'}).then(function (result) {

                        let user = {
                            username: username,
                            password: password,
                            group: cons.GROUND_STAFF_GROUP_NAME
                        };
                        return done(null, user);
                    }).catch(function (err) {
                        done(null, false, req.flash(FAILED_MESSAGE_KEY, "Authentication failed."));
                    });

                }

            } else {
                return done(null, false, req.flash(FAILED_MESSAGE_KEY, 'Invalid username or password'));
            }


        }
));


passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});


router.get('/login', function (req, res) {
    res.render('login', {message: req.flash(FAILED_MESSAGE_KEY)});
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/flight-comment/admin',
    failureRedirect: 'login',
    failureFlash: true
}));
router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/auth/login');

});



module.exports = router;


