"use strict";

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

var jsRequestSchema = mongoose.Schema({
    scheduledFlightDate: Date,
    flightDepartureTime: Date,
    flightArrivalTime : Date,
    flightDepartureTimeUTC: Date,
    flightArrivalTimeUTC: Date,
    operationCarrier: String,
    flightNumber: String,
    origin: String,
    destination: String,
    reqGalacxyId: String,
    isSafetyQualified : Boolean,
    paxName: String,
    paxDOB: Date,
    relationWithReq: String,
    preferredFlight: Boolean,
    created: Date,
    approved: Boolean,
    approveUpdatedDT: Date,
    approverGalacxyId: String,
    priority: Number,
    updatePriorityDT: Date,
    notified: Boolean,
    notifiedDT: Date,
    withdrawn: Boolean,
    withdrawnDT: Date,
    rejected : Boolean,
    rejectedReason: {type: String, default: null},
    rejectedDT: Date,
    precheck: { type: Boolean, default: false},
    isOptOutCabinSeat : { type: Boolean, default: false},
    hasTicket: { type: Boolean, default: false}
});

var JSRequest = mongoose.model('JSRequest', jsRequestSchema);

module.exports = JSRequest;
