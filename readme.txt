# to run test
$ NODE_ENV=yourConfig npm test

# to check syntax with eslint
$ npm run lint

# to generate test document
$ NODE_ENV=yourConfig npm run gen-test-doc

# to generate api document
$ npm run gen-api-doc

=====================================================================

# if APP_ENV should depends on which ENV you wanna build
# for example if build prod, APP_ENV=prod
$ docker build --build-arg APP_ENV=dev -t js-ground-api .
$ docker run -p 49160:8080 -d js-ground-api