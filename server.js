"use strict";

const config = require('config');
process.env.TZ = config.timezone;

const cluster = require('cluster');
// session is used and may be cannot be share within child process
const numOfWorkers = 1;
var {startWorkerServer} = require('./worker-server');
var loggerUtils = require('./app/utils/logger-utils');

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
    loggerUtils.getClusterLogger().info(`Master ${process.pid} is running`);

    // Fork workers.
    for (let i = 0; i < numOfWorkers; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`Worker ${worker.process.pid} died`);
        loggerUtils.getClusterLogger().info(`Worker ${worker.process.pid} died`);
        cluster.fork();
    });
} else {
    return startWorkerServer().then(() => {
        console.log(`Worker ${process.pid} started`);
        loggerUtils.getClusterLogger().info(`Worker ${process.pid} started`);
    });
}
