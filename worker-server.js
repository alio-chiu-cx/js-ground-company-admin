"use strict";

function startWorkerServer() {
    const config = require('config');
    process.env.TZ = config.timezone;

    const fs = require('fs');
    const path = require('path');
    const https = require('https');
    const passport = require('passport');
    const session = require('express-session');
    const flash = require('connect-flash');
    var express = require('express');
    var app = express();
    var bodyParser = require('body-parser');
    const {Tracer} = require('zipkin');
    const CLSContext = require('zipkin-context-cls');
    const zipkinMiddleware = require('zipkin-instrumentation-express').expressMiddleware;
    var cls = require('continuation-local-storage');
    var clsBluebird = require('cls-bluebird');
    const zipkinUtils = require('./app/utils/zipkin-utils');

    return require('./app/utils/db-init').initDb().then(function () {
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({extended: true}));

        // app.use(session({
        //     secret: config.get('authSecret'),
        //     saveUninitialized: true,
        //     resave: true
        // }));
        //
        // app.use(passport.initialize());
        // app.use(passport.session());
        app.use(flash());
        app.use('/static', express.static('public'));
        app.set('view engine', 'ejs');
        app.set('views', [
            path.join('app', 'auth', 'views'),
            path.join('app', 'company', 'views')
        ]);

        const zipkinNamespace = "zipkin";
        const ctxImpl = new CLSContext(zipkinNamespace);
        var ns = cls.getNamespace(zipkinNamespace);
        clsBluebird(ns);

        const recorder = require('./app/utils/zipkin-recorder.js');

        const tracer = new Tracer({ctxImpl, recorder});
        app.use(zipkinMiddleware({
            tracer,
            serviceName: 'js-commit-jumpseat'
        }));
        zipkinUtils.setTracer(tracer);

        let authRouter = require('./app/auth/auth');
        app.use('/auth', authRouter);

        var companyAdmin = require('./app/company/admin');
        app.use('/company/admin',companyAdmin);

        var companyApi = require('./app/company/api');
        app.use('/company', companyApi);

        app.get('/', function (req, res) {
            res.redirect('/company/admin');
        });

        app.get('/web', function (req, res) {
            res.redirect('/company/admin/web');
        });

        const SERVER_PORT = config.get("serverPort");
        if (config.get("certNKey")) {
            https.createServer({
                key: fs.readFileSync(config.get("certNKey.keyPath")),
                cert: fs.readFileSync(config.get("certNKey.certPath")),
                passphrase: config.get("certNKey.passphase")
            }, app).listen(SERVER_PORT, function () {
                console.log('Company-Admin, listening on port ' + SERVER_PORT);
            });
        } else {
            app.listen(SERVER_PORT, function () {
                console.log('Company-Admin, listening on port ' + SERVER_PORT);
            });
        }
    });
}

module.exports = {startWorkerServer};


